Данный проект был сделан в рамках тестового задания

Для запуска проекта необходимо

1) установить зависимости:
```sh
composer install
```
2) выставить рабочую базу данных в .env файле 
3) выполнить миграции:
```sh
php bin/console doctrine:migrations:migrate
```



Страница с фронтом доступна по адресу http(s)://ваш_урл/index.html

Обращение к апи идет по адресу http(s)://ваш_урл/api/

Есть тест для контроллера который можно выполнить через:
```sh
php bin/phpunit
```



