<?php

namespace App\Controller;

use App\Gateway\CurrencyGateway;
use App\UseCase\GetCurrencyUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CurrencyController extends AbstractController
{
    private $getCurrencyUseCase;

    public function __construct(GetCurrencyUseCase $getCurrencyUseCase)
    {
        $this->getCurrencyUseCase = $getCurrencyUseCase;
    }

    public function getCurrency(Request $request): JsonResponse{
        date_default_timezone_set('Europe/Moscow');
        $sort = (int) $request->query->get("sort");
        $date = $request->query->get("date");
        $filterCharCodesArray = $request->query->get("filterCharCodes");

        if (!is_array($filterCharCodesArray)){
            $filterCharCodesArray = [];
        }

        $dateDt = \DateTimeImmutable::createFromFormat("d/m/Y",$date);

        if (is_bool($dateDt)){
            $dateDt = null;
        }


        return new JsonResponse($this->getCurrencyUseCase->execute($sort,$dateDt,$filterCharCodesArray),Response::HTTP_OK);
    }
}