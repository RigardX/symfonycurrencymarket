<?php

namespace App\DataFixtures;

use App\Entity\Valute;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ValuteFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $charCodes = ['USD',"EUR","AZD"];
        $names = ['Доллар','Евро','Армянский драм'];
        $values = [37,42,0.5];
        $valuteIds = ["1","2","3"];
        $createdAt = new \DateTimeImmutable();
        $createdAt = $createdAt->setTime(0,0,0);

        for($i=0;$i<3;$i++){
            $valute = new Valute();
            $valute->setValuteId($valuteIds[$i]);
            $valute->setName($names[$i]);
            $valute->setCharCode($charCodes[$i]);
            $valute->setValue($values[$i]);
            $valute->setCreatedAt($createdAt);
            $valute->setUpdatedAt(new \DateTimeImmutable());

            $manager->persist($valute);
        }

        $values = [32,37,0.3];

        $createdAt = $createdAt->sub(new \DateInterval("P1D"));

        for($i=0;$i<3;$i++){
            $valute = new Valute();
            $valute->setValuteId($valuteIds[$i]);
            $valute->setName($names[$i]);
            $valute->setCharCode($charCodes[$i]);
            $valute->setValue($values[$i]);
            $valute->setCreatedAt($createdAt);
            $valute->setUpdatedAt($createdAt);


            $manager->persist($valute);
        }


        $manager->flush();
    }
}
