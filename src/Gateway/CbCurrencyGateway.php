<?php

namespace App\Gateway;

use App\Gateway\Response\Valute;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CbCurrencyGateway implements CurrencyGateway
{
    private $httpClient;

    public function __construct(HttpClientInterface $cbClient)
    {
        $this->httpClient = $cbClient;
    }

    public function getCurrencyList(\DateTimeImmutable $dateReq): array
    {
        try{
            $res = [];

            $data = $this->httpClient->request('GET','/scripts/XML_daily.asp',[
                "query" => [
                    'date_req' => $dateReq->format("d/m/Y")
                ]
            ]);

            $valutes = new \SimpleXMLElement($data->getContent());

            foreach ($valutes as $valute){
                $res[] = new Valute(
                    $valute->NumCode,
                    $valute->CharCode,
                    $valute->Name,
                    doubleval(str_replace(',', '.',$valute->Value)),
                    $dateReq);
            }

            return $res;
        } catch (ClientException $e){
            return [];
        }
    }
}