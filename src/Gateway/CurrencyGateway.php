<?php

namespace App\Gateway;

use App\Gateway\Response\Valute;
use Doctrine\Common\Collections\ArrayCollection;

interface CurrencyGateway
{

    /**
     * @param \DateTimeImmutable $dateReq
     * @return Valute[]
     */
    public function getCurrencyList(\DateTimeImmutable $dateReq): array;
}