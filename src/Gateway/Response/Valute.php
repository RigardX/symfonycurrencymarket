<?php

namespace App\Gateway\Response;

class Valute
{
    public string $id;
    public string $charCode;
    public string $name;
    public float $value;
    public \DateTimeImmutable $createdAt;

    /**
     * @param string $id
     * @param string $charCode
     * @param string $name
     * @param float $value
     * @param \DateTimeImmutable $createdAt
     */
    public function __construct(string $id, string $charCode, string $name, float $value,\DateTimeImmutable $createdAt)
    {
        $this->id = $id;
        $this->charCode = $charCode;
        $this->name = $name;
        $this->value = $value;
        $this->createdAt = $createdAt;
    }
}