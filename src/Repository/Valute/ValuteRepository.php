<?php

namespace App\Repository\Valute;

use App\Entity\Valute;

interface ValuteRepository
{
    public function findByCreatedDate(\DateTimeImmutable $dateTimeImmutable,?int $sort = 0,?array $filterCharCodes = []): array;
    public function findOneByCharCodeAndCreatedDate(string $charCode,\DateTimeImmutable $createdDate): ?Valute;
    public function save(Valute $entity, bool $flush = false): Valute;
}