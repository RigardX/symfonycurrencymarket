<?php

namespace App\Repository\Valute;

use App\Entity\Valute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Valute>
 *
 * @method Valute|null find($id, $lockMode = null, $lockVersion = null)
 * @method Valute|null findOneBy(array $criteria, array $orderBy = null)
 * @method Valute[]    findAll()
 * @method Valute[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValuteRepositoryImpl extends ServiceEntityRepository implements ValuteRepository
{
    public const SORT_NAME_ASC = 0;
    public const SORT_NAME_DESC = 1;
    public const SORT_VALUE_ASC = 2;
    public const SORT_VALUE_DESC = 3;


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Valute::class);
    }

    public function save(Valute $entity, bool $flush = false): Valute
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $entity;
    }

    public function findByCreatedDate(\DateTimeImmutable $dateTimeImmutable, ?int $sort = 0, ?array $filterCharCodes = []): array
    {
        $qb = $this->createQueryBuilder("c")
            ->where("c.createdAt = :dateNow")
            ->setParameter("dateNow",$dateTimeImmutable);

        if (count($filterCharCodes) > 0){
            $qb->andWhere("c.charCode IN (:charCodes)")
                ->setParameter("charCodes",$filterCharCodes);
        }

        switch ($sort){
            case self::SORT_NAME_DESC:{
                $qb->orderBy("c.name",'DESC');
                break;
            }
            case self::SORT_VALUE_ASC: {
                $qb->orderBy("c.value",'ASC');
                break;
            }
            case self::SORT_VALUE_DESC: {
                $qb->orderBy("c.value",'DESC');
                break;
            }
            case self::SORT_NAME_ASC:
            default:{
                $qb->orderBy("c.name",'ASC');
                break;
            }
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function findOneByCharCodeAndCreatedDate(string $charCode, \DateTimeImmutable $createdDate): ?Valute
    {
        try{
            return $this->createQueryBuilder("c")
                ->where("c.createdAt = :createdDate")
                ->andWhere("c.charCode = :charCode")
                ->setParameter("createdDate",$createdDate)
                ->setParameter("charCode",$charCode)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (\Exception $e){
            return null;
        }
    }
}
