<?php

namespace App\Service\Factory;

use App\Entity\Valute;

class ValuteOrmFactory
{
    public function make(string $id,string $charCode,string $name,string $value,\DateTimeImmutable $createdAt): Valute{
        $valute = new Valute();
        $valute->setValuteId($id);
        $valute->setCharCode($charCode);
        $valute->setName($name);
        $valute->setValue($value);
        $valute->setCreatedAt($createdAt);
        $valute->setUpdatedAt(new \DateTimeImmutable());

        return $valute;
    }
}