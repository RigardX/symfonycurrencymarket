<?php

namespace App\UseCase;

interface GetCurrencyUseCase
{
    public function execute(?int $sort = 0,?\DateTimeImmutable $dateTimeReq = null,?array $filterCharCodesArray = []): array;
}