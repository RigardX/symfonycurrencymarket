<?php

namespace App\UseCase;

use App\Entity\Valute;
use App\Gateway\CurrencyGateway;
use App\Repository\Valute\ValuteRepository;
use App\Service\Factory\ValuteOrmFactory;

class GetCurrencyUseCaseImpl implements GetCurrencyUseCase
{
    const MIN_SECONDS_TO_UPDATE = 10800;
    const MAX_SECONDS_TO_UPDATE = 86400;

    private $currencyGateway;
    private $valuteRepository;
    private $valuteOrmFactory;

    public function __construct(
        CurrencyGateway $currencyGateway,
        ValuteRepository $valuteRepository,
        ValuteOrmFactory $valuteOrmFactory)
    {
        $this->currencyGateway = $currencyGateway;
        $this->valuteRepository = $valuteRepository;
        $this->valuteOrmFactory = $valuteOrmFactory;
    }

    public function execute(?int $sort = 0,?\DateTimeImmutable $dateTimeReq = null,?array $filterCharCodesArray = []): array
    {
        $res = [];
        $dateTimeReq = ($dateTimeReq == null) ?
            new \DateTimeImmutable()
            : $dateTimeReq;


        $dateTimeReq = $dateTimeReq->setTime(0,0,0);
        $now = new \DateTimeImmutable();

        $valutes = $this->valuteRepository->findByCreatedDate($dateTimeReq,$sort,$filterCharCodesArray);

        if (count($valutes) === 0){
            $valutesResponse = $this->currencyGateway->getCurrencyList($dateTimeReq);
            foreach ($valutesResponse as $valute){
                $valuteOrm = $this->valuteOrmFactory->make(
                    $valute->id,
                    $valute->charCode,
                    $valute->name,
                    $valute->value,
                    $dateTimeReq);
                $this->valuteRepository->save($valuteOrm,true);
            }

            $valutes = $this->valuteRepository->findByCreatedDate($dateTimeReq,$sort,$filterCharCodesArray);
        }

        $diffWithUpdatedAndCreated = $now->getTimestamp() - $valutes[0]["updatedAt"]->getTimestamp();

        if ($diffWithUpdatedAndCreated > self::MIN_SECONDS_TO_UPDATE
            && $diffWithUpdatedAndCreated < self::MAX_SECONDS_TO_UPDATE){
            $valutesResponse = $this->currencyGateway->getCurrencyList($dateTimeReq);
            foreach ($valutesResponse as $valute){
               $valuteObj = $this->valuteRepository->findOneByCharCodeAndCreatedDate($valute->charCode,$dateTimeReq);
               if ($valuteObj != null){
                   $valuteObj->setUpdatedAt($now);
                   $valuteObj->setValue($valute->value);
                   $this->valuteRepository->save($valuteObj,true);
               }
            }
            $valutes = $this->valuteRepository->findByCreatedDate($dateTimeReq,$sort,$filterCharCodesArray);
        }


        foreach ($valutes as $valute){
            $res[] = new \App\Gateway\Response\Valute(
                $valute["valuteId"],
                $valute["charCode"],
                $valute["name"],
                $valute["value"],
                $dateTimeReq);
        }

        return $res;
    }
}