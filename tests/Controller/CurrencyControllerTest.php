<?php

namespace App\Tests\Controller;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class CurrencyControllerTest extends ApiTestCase
{
    public function testCurrencyDaily(): void
    {
        $response = static::createClient()->request('GET', '/api/currency');


        $this->assertResponseIsSuccessful();
    }

    public function testCurrencyDailyWithFilter(): void
    {
        $response = static::createClient()->request('GET', '/api/currency?filterCharCodes[]=USD');

        $this->assertResponseIsSuccessful();
        $res = $response->toArray();

        $this->assertCount(1,$res);
        $this->assertEquals("USD",$res[0]["charCode"]);
    }

    public function testCurrencyDailyWithIncorrectFilter(): void
    {
        $response = static::createClient()->request('GET', '/api/currency?filterCharCodes=USD');

        $this->assertResponseIsSuccessful();
        $res = $response->toArray();

        $this->assertGreaterThan(1,count($res));
    }

    public function testCurrencyDailyWithCorrectValueSort(): void{
        $response = static::createClient()->request('GET', '/api/currency?sort=0');

        $this->assertResponseIsSuccessful();
        $res = $response->toArray();



        $this->assertEquals(true,strcmp($res[1]["name"],$res[0]["name"]) === 1);
    }


    public function testCurrencyDailyWithIncorrectValueSort(): void{
        $response = static::createClient()->request('GET', '/api/currency?sort=-1');

        $this->assertResponseIsSuccessful();
        $res = $response->toArray();

        $this->assertEquals(true,strcmp($res[1]["name"],$res[0]["name"]) === 1);
    }

    public function testCurrencyDailyWithAnotherDate(): void{
        $response = static::createClient()->request('GET', '/api/currency?date=25/07/2022');

        $this->assertResponseIsSuccessful();
        $res = $response->toArray();

        $dtExpected = new \DateTimeImmutable();
        $dtExpected = $dtExpected->setDate(2022,07,25);
        $dtExpected = $dtExpected->setTime(0,0,0);

        $dtActual = new \DateTimeImmutable($res[0]["createdAt"]["date"]);

        $this->assertEquals($dtExpected->getTimestamp(),$dtActual->getTimestamp());
    }

    public function testCurrencyDailyWithIncorrectAnotherDate(): void{
        $response = static::createClient()->request('GET', '/api/currency?date=25/0');

        $this->assertResponseIsSuccessful();
        $res = $response->toArray();

        $dtExpected = new \DateTimeImmutable();
        $dtExpected = $dtExpected->setTime(0,0,0);

        $dtActual = new \DateTimeImmutable($res[0]["createdAt"]["date"]);

        $this->assertEquals($dtExpected->getTimestamp(),$dtActual->getTimestamp());
    }
}
